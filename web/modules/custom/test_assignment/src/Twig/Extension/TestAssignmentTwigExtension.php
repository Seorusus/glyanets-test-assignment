<?php

namespace Drupal\test_assignment\Twig\Extension;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Test Assignment Twig extension to add custom functionality.
 */
class TestAssignmentTwigExtension extends AbstractExtension {

  /**
   * {@inheritdoc}
   */
  public function getFunctions(): array {
    return [
      new TwigFunction('get_site_info', [$this, 'getSiteInfo']),
      new TwigFunction('count_published_articles_entity_query', [$this, 'countPublishedArticlesEntityQuery']),
      new TwigFunction('count_published_articles_database', [$this, 'countPublishedArticlesDatabase']),
    ];
  }

  /**
   * Custom function to display site name, current date and time.
   *
   * @return string
   *   The formatted string containing the site name and current date and time.
   */
  public function getSiteInfo(): string {
    $site_name = \Drupal::config('system.site')->get('name');
    $current_datetime = new \DateTime();
    return $site_name . ' - ' . $current_datetime->format('Y-m-d H:i:s');
  }

  /**
   * Counts the number of published nodes of type 'article'.
   *
   * @return int
   *   The number of published article nodes.
   */
  public function countPublishedArticlesEntityQuery(): int {
    $query = \Drupal::entityQuery('node')
      ->condition('status', 1)
      ->condition('type', 'article')
      ->count()
      ->accessCheck(FALSE);
    return $query->execute();
  }

  /**
   * Counts the number of published nodes of type 'article' using database query.
   *
   * @return int
   *   The number of published article nodes.
   */
  public function countPublishedArticlesDatabase(): int {
    $connection = \Drupal::database();
    $query = $connection->select('node_field_data', 'n')
      ->fields('n', ['nid'])
      ->condition('n.status', 1)
      ->condition('n.type', 'article')
      ->countQuery();
    return $query->execute()->fetchField();
  }

}
