<?php

namespace Drupal\test_assignment;

use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Class ContentTypeCreator.
 *
 * Provides functionality for creating custom content types and fields programmatically.
 * It is designed to ensure that the 'test_node' content type is created with a specific field
 * to store current date and time, and also to facilitate the creation of nodes of this content type.
 */
class ContentTypeCreator {

  /**
   * The entity type manager.
   *
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new ContentTypeCreator object.
   *
   * @param EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Creates the 'test_node' content type if it does not exist.
   * @throws EntityStorageException
   */
  public function createContentType(): void {
    $storage = $this->entityTypeManager->getStorage('node_type');

    if (!$storage->load('test_node')) {
      $node_type = $storage->create([
        'type' => 'test_node',
        'name' => 'Test Node',
        'description' => 'A content type for test nodes.',
      ]);
      $node_type->save();

      // After creating the content type, create the 'current_date' field.
      $this->createCurrentDateField();
    }
  }

  /**
   * Creates the Current Date Field.
   * @throws EntityStorageException
   */
  protected function createCurrentDateField(): void {
    $field_storage_name = 'field_current_date';
    $field_storage = FieldStorageConfig::loadByName('node', $field_storage_name);

    // Check if the field storage already exists.
    if (!$field_storage) {
      FieldStorageConfig::create([
        'field_name' => $field_storage_name,
        'entity_type' => 'node',
        'type' => 'datetime',
        'settings' => [
          'datetime_type' => 'datetime',
        ],
      ])->save();
    }

    $field_name = 'field_current_date';
    $field = FieldConfig::loadByName('node', 'test_node', $field_name);

    // Check if the field already exists on the 'test_node' content type.
    if (!$field) {
      FieldConfig::create([
        'field_name' => $field_name,
        'label' => 'Current Date',
        'entity_type' => 'node',
        'bundle' => 'test_node',
        'settings' => [],
        'required' => FALSE,
      ])->save();
    }

    // Assign form and view display settings for the 'current_date' field.
    $entity_form_display = \Drupal::entityTypeManager()->getStorage('entity_form_display')->load('node.test_node.default');
    if (!$entity_form_display) {
      $entity_form_display = \Drupal::entityTypeManager()->getStorage('entity_form_display')->create([
        'targetEntityType' => 'node',
        'bundle' => 'test_node',
        'mode' => 'default',
        'status' => TRUE,
      ]);
    }
    $entity_form_display->setComponent($field_storage_name, [
      'type' => 'datetime_default',
      'settings' => [
        'date_format' => 'd.m.Y H:i',
      ],
    ])->save();

    $entity_view_display = \Drupal::entityTypeManager()->getStorage('entity_view_display')->load('node.test_node.default');
    if (!$entity_view_display) {
      $entity_view_display = \Drupal::entityTypeManager()->getStorage('entity_view_display')->create([
        'targetEntityType' => 'node',
        'bundle' => 'test_node',
        'mode' => 'default',
        'status' => TRUE,
      ]);
    }
    $entity_view_display->setComponent($field_storage_name, [
      'type' => 'datetime_default',
      'label' => 'hidden',
      'settings' => [
        'date_format' => 'd.m.Y H:i',
      ],
    ])->save();
  }

}
