<?php

namespace Drupal\test_assignment;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Class EntityDumper.
 *
 * Provides a service for dumping entity data.
 */
class EntityDumper {

  /**
   * The entity type manager service.
   *
   * @var EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs a new EntityDumper object.
   *
   * @param EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service, used for loading entity data.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Dumps the data of a specified entity.
   *
   * @param string $entity_type
   *   The type of the entity to dump.
   * @param int|string $entity_id
   *   The ID of the entity to dump.
   *
   * @return string
   *   A string containing the HTML-safe dump of the entity's data.
   *
   * @throws InvalidPluginDefinitionException
   *   Thrown if the entity type does not exist.
   *
   * @throws PluginNotFoundException
   *   Thrown if the entity type cannot be found.
   */
  public function dumpEntity(string $entity_type, int|string $entity_id): string {
    $entity = $this->entityTypeManager->getStorage($entity_type)->load($entity_id);
    if ($entity) {
      $output = print_r($entity->toArray(), TRUE);
      return '<pre>' . htmlspecialchars($output, ENT_QUOTES | ENT_HTML5) . '</pre>';
    } else {
      return "Entity with ID $entity_id not found.";
    }
  }

}
