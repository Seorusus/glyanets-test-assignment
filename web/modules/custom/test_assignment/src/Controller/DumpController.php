<?php

namespace Drupal\test_assignment\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\test_assignment\EntityDumper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;

/**
 * Controller for handling the dumping of entity data. *
 */
class DumpController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The EntityDumper service.
   *
   * @var EntityDumper
   */
  protected EntityDumper $entityDumper;

  /**
   * Creates an instance of the DumpController.
   *
   * @param ContainerInterface $container
   *   The service container.
   *
   * @return static
   *   A new instance of the DumpController.
   */
  public static function create(ContainerInterface $container): DumpController|static {
    return new static(
      $container->get('test_assignment.entity_dumper')
    );
  }

  /**
   * Constructs a new DumpController object.
   *
   * @param EntityDumper $entityDumper
   *   The EntityDumper service.
   */
  public function __construct(EntityDumper $entityDumper) {
    $this->entityDumper = $entityDumper;
  }

  /**
   * Dumps data for the specified node.
   *
   * @param int|string $node_id
   *   The ID of the node to dump.
   *
   * @return Response
   *   A simple response indicating where to find the dump output.
   */
  public function dumpNode(int|string $node_id): Response {
    $dumpOutput = $this->entityDumper->dumpEntity('node', $node_id);
    // Returns Dump on the page.
    return new Response($dumpOutput);
  }

}
