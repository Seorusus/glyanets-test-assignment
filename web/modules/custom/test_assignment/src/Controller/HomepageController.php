<?php

namespace Drupal\test_assignment\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Controller for the homepage.
 */
class HomepageController extends ControllerBase {

  /**
   * Returns content for the homepage.
   *
   * @return array
   *   A render array for Drupal.
   */
  public function content(): array {
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Welcome to the home page'),
    ];
  }
}
