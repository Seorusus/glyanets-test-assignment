<?php

namespace Drupal\test_assignment\Plugin\views\field;

use Drupal\Component\Render\MarkupInterface;
use Drupal\node\NodeInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * A handler to provide a field that displays the node title and creation date.
 *
 * @ViewsField("test_assignment_node_title_date")
 */
class NodeTitleDateField extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Empty to avoid a query on this field.
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values): MarkupInterface|string {
    $node = $values->_entity;
    if ($node instanceof NodeInterface) {
      $date = \Drupal::service('date.formatter')->format($node->getCreatedTime(), 'custom', 'd.m.Y');
      return $node->getTitle() . ' - ' . $date;
    }
    return '';
  }
}
