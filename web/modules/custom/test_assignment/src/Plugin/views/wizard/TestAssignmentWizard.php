<?php

namespace Drupal\test_assignment\Plugin\views\wizard;

use Drupal\views\Plugin\views\wizard\WizardPluginBase;

/**
 * Provides a wizard for creating Test Assignment views.
 *
 * @ViewsWizard(
 *   id = "test_assignment_wizard",
 *   base_table = "test_assignment",
 *   title = @Translation("Test Assignment Wizard")
 * )
 */
class TestAssignmentWizard extends WizardPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function defaultDisplayOptions(): array {
    $display_options = parent::defaultDisplayOptions();

    // ID field default settings.
    $display_options['fields']['id'] = [
      'table' => 'test_assignment',
      'field' => 'id',
      'provider' => 'test_assignment',
      'label' => '',
      'exclude' => TRUE,
      'alter' => [
        'alter_text' => FALSE,
        'text' => '',
        'make_link' => FALSE,
        'path' => '',
        'link_to_entity' => FALSE,
      ],
      'element_label_colon' => FALSE,
      'element_default_classes' => TRUE,
      'empty' => '',
      'hide_empty' => FALSE,
      'empty_zero' => FALSE,
      'hide_alter_empty' => TRUE,
      'plugin_id' => 'field',
    ];

    // NID field default settings.
    $display_options['fields']['nid'] = [
      'table' => 'test_assignment',
      'field' => 'nid',
      'provider' => 'test_assignment',
      'label' => t('Node ID'),
      'plugin_id' => 'field',
    ];

    // Data field default settings.
    $display_options['fields']['data'] = [
      'table' => 'test_assignment',
      'field' => 'data',
      'provider' => 'test_assignment',
      'label' => t('Data'),
      'plugin_id' => 'field',
    ];


    // Status field settings.
    $display_options['fields']['status'] = [
      'table' => 'test_assignment',
      'field' => 'status',
      'provider' => 'test_assignment',
      'label' => t('Status'),
      'plugin_id' => 'boolean',
      'settings' => [
        'format' => 'custom',
        'format_custom_true' => t('Published'),
        'format_custom_false' => t('Unpublished'),
      ],
    ];

    // Default filter.
    $display_options['filters']['status'] = [
      'value' => 1,
      'table' => 'test_assignment',
      'field' => 'status',
      'provider' => 'test_assignment',
      'plugin_id' => 'boolean',
      'label' => t('Published'),
    ];

    // Default sort.
    $display_options['sorts']['id'] = [
      'order' => 'DESC',
      'table' => 'test_assignment',
      'field' => 'id',
      'provider' => 'test_assignment',
      'plugin_id' => 'standard',
    ];

    return $display_options;
  }

}
