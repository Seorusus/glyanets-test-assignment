<?php

namespace Drupal\test_assignment\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\node\Entity\Node;

/**
 * Processes tasks for republishing articles.
 *
 * @QueueWorker(
 *   id = "republish_articles",
 *   title = @Translation("Republish Articles"),
 *   cron = {"time" = 60}
 * )
 */
class RepublishArticles extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
    $nid = $data->nid;
    $node = Node::load($nid);
    if ($node instanceof Node && $node->getType() == 'article') {
      $changed_time = $node->getChangedTime();
      // 86400 seconds in 24 hours
      if (time() - $changed_time > 86400) {
        $node->save();
      }
    }
  }

}
