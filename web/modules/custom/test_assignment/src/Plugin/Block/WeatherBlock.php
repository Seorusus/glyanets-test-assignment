<?php

namespace Drupal\test_assignment\Plugin\Block;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\test_assignment\WeatherService;

/**
 * Provides a 'Weather Block'.
 *
 * @Block(
 *   id = "weather_block",
 *   admin_label = @Translation("Weather Block"),
 * )
 */
class WeatherBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The current user.
   *
   * @var AccountProxyInterface
   */
  protected AccountProxyInterface $currentUser;

  /**
   * The weather service.
   *
   * @var WeatherService
   */
  protected WeatherService $weatherService;

  /**
   * Constructs a new WeatherBlock instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param AccountProxyInterface $current_user
   *   The current user service.
   * @param WeatherService $weather_service
   *   The weather service to fetch weather data.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AccountProxyInterface $current_user, WeatherService $weather_service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentUser = $current_user;
    $this->weatherService = $weather_service;
  }

  /**
   * Creates an instance of the WeatherBlock.
   *
   * @param ContainerInterface $container
   *   The container service.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   *
   * @return static
   *   Returns an instance of this block class.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): WeatherBlock|static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('test_assignment.weather_service')
    );
  }

  /**
   * Builds the weather block content.
   *
   * @return array
   *   A render array representing the weather information.
   */
  public function build(): array {
    $content = $this->weatherService->getWeather();
    $weatherData = Json::decode($content);

    if ($weatherData && isset($weatherData['current'])) {
      $description = $weatherData['current']['weather'][0]['description'];
      // Convert temperature Kelvin to Celsius degrees.
      $temp = $weatherData['current']['temp'] - 273.15;
      $feelsLike = $weatherData['current']['feels_like'] - 273.15;
      $pressure = $weatherData['current']['pressure'];
      $humidity = $weatherData['current']['humidity'];
      $windSpeed = $weatherData['current']['wind_speed'];
      $windDeg = $weatherData['current']['wind_deg'];

      $formattedWeather = [
        '#markup' => $this->t('Current weather in Kyiv:<br/> <strong>@description</strong>, Temp: <strong>@temp°C</strong>, Feels like: <strong>@feelsLike°C</strong>, Pressure: <strong>@pressure hPa</strong>, Humidity: <strong>@humidity%</strong>, Wind: <strong>@windSpeed m/s</strong>, Wind direction: <strong>@windDeg°</strong>', [
          '@description' => $description,
          '@temp' => number_format($temp, 2),
          '@feelsLike' => number_format($feelsLike, 2),
          '@pressure' => $pressure,
          '@humidity' => $humidity,
          '@windSpeed' => $windSpeed,
          '@windDeg' => $windDeg,
        ]),
        '#allowed_tags' => [
          'strong',
          'br/'
          ],
      ];
    } else {
      $formattedWeather = $this->t('Unable to fetch weather data.');
    }

    return [
      $formattedWeather,
    ];
  }

  public function getCacheMaxAge(): int {
    // Disable cache for anonymous users
    return $this->currentUser->isAnonymous() ? 0 : 3600;
  }
}
