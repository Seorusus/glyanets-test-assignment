<?php

namespace Drupal\test_assignment\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;

/**
 * Provides a Test Assignment Random form.
 */
class TestAssignmentRandomForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'test_assignment.random_settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'test_assignment_random_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('test_assignment.random_settings');

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $config->get('enabled'),
    ];

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#default_value' => $config->get('title'),
    ];

    $form['node'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Reference node'),
      '#target_type' => 'node',
      '#default_value' => $config->get('node') ? Node::load($config->get('node')) : NULL,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('test_assignment.random_settings')
      ->set('enabled', $form_state->getValue('enabled'))
      ->set('title', $form_state->getValue('title'))
      ->set('node', $form_state->getValue('node'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}
