<?php

namespace Drupal\test_assignment\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;

/**
 * Provides a Test Assignment Result form.
 */
class TestAssignmentResultForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'test_assignment_result_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $form['text_test'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text Test'),
      '#description' => $this->t('Enter test text here.'),
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'button',
      '#value' => $this->t('Submit'),
      '#ajax' => [
        'callback' => '::submitAjax',
        'wrapper' => 'text-test-result',
        'effect' => 'fade',
      ],
    ];

    $form['result'] = [
      '#type' => 'markup',
      '#markup' => '<div id="text-test-result"></div>',
    ];

    return $form;
  }

  /**
   * AJAX callback handler for the submit button.
   */
  public function submitAjax(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $resultText = $form_state->getValue('text_test');
    $response->addCommand(new HtmlCommand('#text-test-result', $this->t('Result: @result', ['@result' => $resultText])));
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // The submit handler is not used in this AJAX example, but it is required by the Form API.
  }

}
