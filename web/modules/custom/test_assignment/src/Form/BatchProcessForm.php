<?php

namespace Drupal\test_assignment\Form;

use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;

/**
 * Provides a Batch Process form.
 */
class BatchProcessForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'test_assignment_batch_process_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Start Batch Process'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $node_ids = \Drupal::entityQuery('node')
      ->condition('type', 'article')
      ->accessCheck(FALSE)
      ->execute();

    $batch = [
      'title' => $this->t('Resaving all article nodes...'),
      'operations' => [],
      'finished' => '\Drupal\test_assignment\Form\BatchProcessForm::batchFinished',
    ];

    foreach ($node_ids as $node_id) {
      $batch['operations'][] = [
        '\Drupal\test_assignment\Form\BatchProcessForm::batchOperation',
        [$node_id]
      ];
    }

    batch_set($batch);
  }

  /**
   * Batch operation callback: loads and saves a node.
   *
   * @param int $node_id
   *   The node ID.
   * @param array $context
   *   The batch context array.
   */
  public static function batchOperation(int $node_id, array &$context): void {
    $node = Node::load($node_id);
    if ($node) {
      try {
        $node->set('changed', \Drupal::time()->getRequestTime());

        $node->save();
        $context['results'][] = $node_id;
        $context['message'] = t('Resaved node @nid', ['@nid' => $node_id]);
      } catch (EntityStorageException $e) {
        \Drupal::logger('test_assignment')->error('Error saving node @nid: @error', [
          '@nid' => $node_id,
          '@error' => $e->getMessage(),
        ]);
      }
    }
  }

  /**
   * Callback for batch completion: logs the result.
   *
   * @param bool $success
   *   Indicates if the batch process was successful.
   * @param array $results
   *   An array of batch results.
   * @param array $operations
   *   An array of remaining operations (if any).
   */
  public static function batchFinished(bool $success, array $results, array $operations): void {
    if ($success) {
      $message = \Drupal::translation()->formatPlural(
        count($results),
        'One node processed.',
        '@count nodes processed.'
      );
    }
    else {
      $message = t('Finished with an error.');
    }
    \Drupal::messenger()->addMessage($message);
  }
}
