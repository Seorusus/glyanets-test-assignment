<?php

namespace Drupal\test_assignment;

use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\DateTime\DrupalDateTime;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\path_alias\Entity\PathAlias;


/**
 * Class NodeCreator.
 *
 * Service for creating nodes of the 'test_node' content type.
 */
class NodeCreator {

  use StringTranslationTrait;


  /**
   * The entity type manager.
   *
   * @var EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs a new NodeCreator object.
   *
   * @param EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Creates a node of the 'test_node' content type with the current datetime.
   * @throws EntityStorageException
   */
  public function createTestNode(): void {
    $node_storage = $this->entityTypeManager->getStorage('node');

    // Get current datetime in ISO 8601 format.
    $current_datetime = new DrupalDateTime();
    $formatted_datetime = $current_datetime->format('Y-m-d\TH:i:s');

    $node = $node_storage->create([
      'type' => 'test_node',
      'title' => $this->t('First Test Node'),
      'field_current_date' => $formatted_datetime,
      'uid' => 1,
    ]);

    $node->save();

    $node_id = $node->id();

    // Creates URL alias.
    if ($node_id) {
      $path_alias = PathAlias::create([
        'path' => '/node/' . $node->id(),
        'alias' => '/my-first-page',
        'language' => \Drupal::languageManager()->getCurrentLanguage()->getId(),
      ]);
      $path_alias->save();    }

    // Saves the node ID.

    \Drupal::state()->set('test_assignment.latest_node_id', $node_id);
  }
}
