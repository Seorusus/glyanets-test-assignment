<?php

namespace Drupal\test_assignment;

use Drupal\Core\Cache\CacheBackendInterface;
use GuzzleHttp\ClientInterface;

/**
 * Class WeatherService
 *
 * Service class to fetch weather information from the OpenWeatherMap API.
 */
class WeatherService {

  /**
   * The HTTP client to send requests to the API.
   *
   * @var ClientInterface
   */
  protected ClientInterface $httpClient;

  /**
   * The cache backend interface.
   *
   * @var CacheBackendInterface
   */
  protected CacheBackendInterface $cacheBackend;

  /**
   * Constructs a WeatherService object.
   *
   * @param ClientInterface $http_client
   *   The HTTP client to use for requests.
   * @param CacheBackendInterface $cache_backend
   *   The cache backend to use for caching weather data.
   */
  public function __construct(ClientInterface $http_client, CacheBackendInterface $cache_backend) {
    $this->httpClient = $http_client;
    $this->cacheBackend = $cache_backend;
  }

  /**
   * Fetches the current weather data from OpenWeatherMap API.
   *
   * @return string
   *   The weather data in JSON format, or a JSON encoded error message.
   *   Attempts to return cached data if available to avoid unnecessary API calls.
   */
  public function getWeather(): string {
    // Cache ID.
    $cid = 'weather_data:kyiv';

    // Attempt to get cached weather data.
    if ($cache = $this->cacheBackend->get($cid)) {
      return $cache->data;
    } else {
      // If cache is empty or expired, fetch new data from the API.
      $apiKey = '910c86c97d2ecc3cdc899c5bad71b32b';
      $lat = '50.4501';
      $lon = '30.5234';
      $url = "https://api.openweathermap.org/data/3.0/onecall?lat={$lat}&lon={$lon}&appid={$apiKey}";

      try {
        $response = $this->httpClient->request('GET', $url);
        $data = $response->getBody()->getContents();

        // Cache the new data for 1 hour.
        $this->cacheBackend->set($cid, $data, time() + 3600);

        return $data;
      } catch (\Exception $e) {
        // In case of any error, return a JSON encoded error message.
        return json_encode(['error' => 'Unable to fetch weather data.']);
      }
    }
  }

}
