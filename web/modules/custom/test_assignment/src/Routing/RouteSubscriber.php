<?php

namespace Drupal\test_assignment\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * Alters existing routes.
   *
   * @param RouteCollection $collection
   *   The collection of routes.
   */
  protected function alterRoutes(RouteCollection $collection): void {
    // Check if the homepage route exists.
    if ($route = $collection->get('test_assignment.homepage')) {
      // Change the access requirements for the route.
      $route->setRequirement('_user_is_logged_in', 'TRUE');
    }
  }
}
