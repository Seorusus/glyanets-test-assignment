<?php

use Drupal\node\Entity\Node;

/**
 * Implements hook_install().
 * @throws Exception
 */
function test_assignment_install(): void {
  // Creates test_node content type.
  \Drupal::service('test_assignment.content_type_creator')->createContentType();

  // Creates node of test_node content type.
  \Drupal::service('test_assignment.node_creator')->createTestNode();

  // Gets list of Article nodes.
  $nids = \Drupal::entityQuery('node')
    ->condition('type', 'article')
    ->condition('status', 1)
    ->accessCheck(FALSE)
    ->execute();

  if (!empty($nids)) {
    // Loads node entities.
    $nodes = Node::loadMultiple($nids);

    // DB connects.
    $connection = \Drupal::database();
    foreach ($nodes as $node) {
      // Converting data to BLOB.
      $blob_data = serialize($node->getTitle());

      // Adds to table.
      $connection->insert('test_assignment')
        ->fields([
          'nid' => $node->id(),
          'data' => $blob_data,
          'status' => $node->isPublished() ? 1 : 0,
        ])
        ->execute();
    }
  }
}

/**
 * Implements hook_uninstall().
 * @throws \Drupal\Core\Entity\EntityStorageException
 */
function test_assignment_uninstall(): void {
  // Deletes all nodes of the 'test_node' CT.
  $nids = \Drupal::entityQuery('node')
    ->condition('type', 'test_node')
    ->accessCheck(FALSE)
    ->execute();

  if ($nids) {
    $nodes = Node::loadMultiple($nids);
    foreach ($nodes as $node) {
      $node->delete();
    }
  }

  // Deletes CT 'test_node'.
  $content_type = \Drupal::entityTypeManager()->getStorage('node_type')->load('test_node');
  $content_type?->delete();
}

/**
 * Implements hook_schema().
 */
function test_assignment_schema(): array {
  $schema['test_assignment'] = [
    'description' => 'The base table for test_assignment.',
    'fields' => [
      'id' => [
        'description' => 'The primary ID.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'nid' => [
        'description' => 'The node ID.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'data' => [
        'description' => 'The blob data.',
        'type' => 'blob',
        'not null' => FALSE,
        'size' => 'big',
      ],
      'status' => [
        'description' => 'The status.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'size' => 'tiny',
      ],
    ],
    'indexes' => [
      'nid' => ['nid'],
      'status' => ['status'],
    ],
    'primary key' => ['id'],
  ];

  return $schema;
}
