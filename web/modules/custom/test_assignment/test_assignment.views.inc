<?php

/**
 * Implements hook_views_data().
 */
function test_assignment_views_data(): array {
  $data = [];

  // Defines the output of a custom field test_assignment_node_title_date.
  $data['node_field_data']['test_assignment_node_title_date'] = [
    'title' => t('Node title and date'),
    'help' => t('Displays the node title and creation date.'),
    'field' => [
      'id' => 'test_assignment_node_title_date',
    ],
  ];

  // Table description.
  $data['test_assignment']['table']['group'] = t('Test Assignment');
  $data['test_assignment']['table']['base'] = [
    'field' => 'id',
    'title' => t('Test Assignment'),
    'help' => t('Test Assignment contains custom data.'),
    'weight' => -10,
  ];

  // ID field.
  $data['test_assignment']['id'] = [
    'title' => t('ID'),
    'help' => t('The ID of the Test Assignment record.'),
    'field' => [
      'id' => 'numeric',
    ],
    'filter' => [
      'id' => 'numeric',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  // NID field.
  $data['test_assignment']['nid'] = [
    'title' => t('Node ID'),
    'help' => t('The Node ID related to the Test Assignment.'),
    'field' => [
      'id' => 'numeric',
    ],
    'filter' => [
      'id' => 'numeric',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'relationship' => [
      'base' => 'node_field_data',
      'base field' => 'nid',
      'id' => 'standard',
      'label' => t('Related node'),
    ],
  ];

  // Data field.
  $data['test_assignment']['data'] = [
    'title' => t('Data'),
    'help' => t('The Blob data associated with the Test Assignment.'),
    'field' => [
      'id' => 'standard',
    ],
  ];

  // Status field.
  $data['test_assignment']['status'] = [
    'title' => t('Status'),
    'help' => t('The published status of the nodes.'),
    'field' => [
      'id' => 'boolean',
      'label' => t('Published status'),
      'type' => 'yes-no',
    ],
    'filter' => [
      'id' => 'boolean',
      'label' => t('Published'),
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  return $data;
}
