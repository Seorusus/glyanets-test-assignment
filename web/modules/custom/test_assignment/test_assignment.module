<?php

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;

/**
 * Implements hook_entity_view_alter().
 */
function test_assignment_entity_view_alter(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display): void {
  // Gets the node ID.
  $latest_node_id = \Drupal::state()->get('test_assignment.latest_node_id');

  // Checks the 'test_node' CT, node ID.
  if ($entity->getEntityTypeId() == 'node' && $entity->bundle() == 'test_node' && $entity->id() == $latest_node_id) {
    // Disables caching for a node.
    $build['#cache']['max-age'] = 0;
  }
}

/**
 * Implements hook_entity_presave().
 */
function test_assignment_entity_presave(EntityInterface $entity): void {
  // Checks if the node is article.
  if ($entity instanceof Node && $entity->bundle() === 'article') {
    $current_date = date('d.m.Y');
    // Checks to ensure that the date is not re-added each time you save..
    if (!preg_match('/^\d{2}\.\d{2}\.\d{4} — /', $entity->getTitle())) {
      $new_title = $current_date . ' — ' . $entity->getTitle();
      $entity->setTitle($new_title);
    }
  }
}

/**
 * Implements hook_page_attachments().
 */
function test_assignment_page_attachments(array &$attachments): void {
  if (\Drupal::routeMatch()->getRouteName() == 'entity.node.canonical') {
    $node = \Drupal::routeMatch()->getParameter('node');
    if ($node instanceof NodeInterface && $node->bundle() == 'article') {
      $attachments['#attached']['library'][] = 'test_assignment/current_date';
    }
  }

  if (\Drupal::routeMatch()->getRouteName() == 'view.test_assignment_autosubmit.page_1') {
    $attachments['#attached']['library'][] = 'test_assignment/auto-submit';
  }
}

/**
 * Implements hook_theme().
 */
function test_assignment_theme($existing, $type, $theme, $path): array {
  return [
    'hello_world_template' => [
      'variables' => ['site_name' => NULL],
    ],
  ];
}

/**
 * Implements hook_cron().
 */
function test_assignment_cron(): void {
  // Gets current time.
  $current_time = time();

  // Gets node NID list for updating.
  $query = \Drupal::entityQuery('node')
    ->condition('type', 'article')
    ->condition('changed', $current_time - 86400, '<')
    ->accessCheck(FALSE)
    ->execute();

  // Adds nodes to the queue.
  if (!empty($query)) {
    $queue = \Drupal::queue('republish_articles');
    foreach ($query as $nid) {
      $queue->createItem((object) ['nid' => $nid]);
    }
  }
}
