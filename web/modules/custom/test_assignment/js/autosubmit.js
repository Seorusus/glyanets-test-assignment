(function ($, Drupal) {
  Drupal.behaviors.autoSubmitPublishedStatus = {
    attach: function (context, settings) {
      // Checks whether the element has already been processed.
      $('.views-exposed-form select[name="published_status"]', context).each(function () {
        if (!$(this).attr('data-autosubmit-bound')) {
          $(this).attr('data-autosubmit-bound', '1').change(function () {
            // Autosubmit form by change status.
            $(this).closest('form').find('.form-submit').click();
          });
        }
      });
    }
  };
})(jQuery, Drupal);
