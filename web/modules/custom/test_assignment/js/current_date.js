document.addEventListener('DOMContentLoaded', function() {
  // Gets the element where to add the current date and time.
  var header = document.querySelector('.site-header');
  if (header) {
    // Creates an element for displaying the current date and time.
    var dateElement = document.createElement('div');
    dateElement.className = 'current-date';
    header.appendChild(dateElement); // Adds the date and time element to the header.

    // Function to update the date and time display.
    function updateDateTime() {
      var now = new Date();
      var dateString = now.toLocaleDateString('uk-UA') + ' ' + now.toLocaleTimeString('uk-UA');
      dateElement.textContent = Drupal.t('Current date and time: ') + dateString;
    }

    // Calls the updateDateTime function every second (1000 milliseconds).
    setInterval(updateDateTime, 1000);

    // Also updates the date and time immediately on load.
    updateDateTime();
  }
});
