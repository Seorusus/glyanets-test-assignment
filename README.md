# Glyanets Test assignment


***

Created by Oleg Kreminsky seorusus@gmail.com

***

# Test Cases

## Drupal 10 Installation via Composer
- **Action**: Clone the project into the local repository's project folder using `git clone git@gitlab.com:Seorusus/glyanets-test-assignment.git` and then execute `composer install`.
- **Expected Result**: The project is successfully installed, with all dependencies resolved.

## Programmatic Creation of a Node of Type test_node
- **Action**: A node of type `test_node` is created by the `test_assignment` module.
- **Expected Result**: The node is successfully created and can be found in the Drupal administrative interface.

## Creation of the /my-first-page with the Current Date and Time
- **Action**: The path `/my-first-page` is created by the `test_assignment` module.
- **Expected Result**: Navigating to `/my-first-page` displays the current date and time in the specified format, with no page caching.

## Adding the Current Date to the Title of Article Posts
- **Action**: The `test_assignment` module adds the current date to the title of each `article` type post upon creation or saving.
- **Expected Result**: Upon creation or updating, the article's title includes the current date in the specified format.

## Adding Live Date and Time to the Article Page Using JS
- **Action**: The `test_assignment` module implements JavaScript to add live date and time to the article page.
- **Expected Result**: Each article page's header displays the current date and time, updating in real-time.

## Programmatic Route Permission Change for /homepage
- **Action**: The `test_assignment` module changes access permissions for the path `/homepage`, defined as the site's homepage, to allow viewing by authenticated users only.
- **Expected Result**: Anonymous users attempting to access `/homepage` are redirected to the login page or shown an error message.

## Custom Twig Template and Block Creation
- **Action**: Within the `test_assignment` module, create a custom Twig template `hello-world-template.html.twig` and a block to display it on the site's homepage as "HelloWorld - %Site Name%".
- **Expected Result**: The homepage displays content defined in `hello-world-template.html.twig`, including "HelloWorld - %Site Name%".

## Creating a Block Displaying the Current Route Name
- **Action**: The `test_assignment` module creates a block that displays the name of the current route.
- **Expected Result**: The block on the homepage displays the unique name of the route corresponding to the current page.

## Creating a Link in the Toolbar
- **Action**: The `test_assignment` module adds a link to the administrative toolbar, leading to the creation page for a node of type `article`.
- **Expected Result**: A new link appears in the administrative toolbar next to the "Configuration" section, leading to the article creation page.

## Testing the test_assignment_result Form
- **Action**: Navigate to `/test-assignment-result`, enter text into the `text_test` field, and press the submit button.
- **Expected Result**: The text "Result: %entered data%" appears below the submit button. Results are updated asynchronously without page reload.

## Testing the Twig Function
- **Action**: Navigate to the homepage.
- **Expected Result**: The "Hello World block" on the homepage displays the site name, current date, and time.

## Counting Nodes of Type Article
- **Action**: Ensure that nodes of type `article` are created in the system and navigate to the homepage.
- **Expected Result**: The template displays the number of published `article` nodes, counted using `entityQuery` and `Drupal::database`.

## Checking Creation and Structure of the test_assignment Table
- **Action**: The `test_assignment` module creates the `test_assignment` table. Use the database interface or command line to check the presence and structure of the `test_assignment` table.
- **Expected Result**: The `test_assignment` table exists and contains fields `id`, `nid`, `data`, `status` with appropriate data types and indexes.

## Checking Creation of a Field for Views
- **Action**: The `test_assignment` module creates the View `test_assignment`. Verify that a new field displaying the node's title and creation date is available in the Views field settings. Navigate to the view page `/test-assignment-articles` and check the results.
- **Expected Result**: The `test_assignment` View includes a field that correctly displays the node's title and creation date. The page `/test-assignment-articles` shows a list of nodes with titles and creation dates using the specified field.

## Testing the Wizard for the test_assignment Table
- **Action**: The `test_assignment` module creates a views wizard. Navigate to `/wizard`.
- **Expected Result**: Data from the `test_assignment` table is displayed on the page using the Views wizard.

## QueueWorker for Resaving Nodes
- **Action**: Run cron after some article nodes' data were modified more than 24 hours ago.
- **Expected Result**: Article nodes that have not been updated for over 24 hours are automatically resaved, updating their last modification date.

## test_assignment_random Form
- **Action**: Navigate to `/test-assignment-random` and check for a form with fields: Enabled (checkbox), Title (textfield), Node (entity reference to a node).
- **Expected Result**: The form is correctly displayed with the specified fields. Upon filling out the form and pressing the submit button, data is saved in the configuration.

## Autosubmit for views test_assignment_autosubmit
- **Action**: Open the page `/test-assignment-autosubmit` with an exposed form containing the field Published "Yes/No". Change the select value.
- **Expected Result**: After changing the select value, the form automatically submits using AJAX, and filter results are displayed without reloading the page.

## Service with a Method for Dumping Entities
- **Action**: Navigate to `/dump-node/{node_id}`, where `{node_id}` is the identifier of an existing article node.
- **Expected Result**: The page displays a dump of the requested node's data, including all its fields and values.

## Form with Batch Process for Resaving Nodes
- **Action**:
  - Navigate to `/batch-articles`.
  - Press the "Start Batch Process" button to initiate the batch process for resaving all nodes of type article.
- **Expected Result**:
  - The batch process starts, displaying progress for 50 operations (if there are enough article nodes). Each operation resaves one node.
  - Upon completion of all operations, the user is notified of the successful completion of the batch process. All article nodes have been resaved, which can be verified by the date of the last update in the administrative interface.

## Weather Block Using OpenWeather API
- **Action**:
  - Navigate to any custom page of the site.
  - Check for the presence of the weather block in the Header region.
- **Expected Result**:
  - The weather block is displayed in the specified region on all pages of the site.
  - Weather data matches the current conditions for the location Kiev.
  - When the page is refreshed within an hour, the weather data does not change.
  - Caching is disabled for anonymous users.

